import psycopg2
from data import postgres_service
from classes import hitter, person, fielder


class PeopleService:
    def _init__(self):
        pass

    def search_by_name(self, first_name, last_name):
        """ Search by Name """
        id = '0'
        db_service = postgres_service.PostgresService()
        DATABASE_URL = db_service.get_connection_string()
        print(DATABASE_URL)
        conn = psycopg2.connect(DATABASE_URL, sslmode='require')
        try:
            query = "SELECT playerid "
            query += "FROM public.people WHERE nameFirst = '{}' and nameLast = '{}';".format(first_name,
                                                                                             last_name)

            cur = conn.cursor()
            cur.execute(query)
            row = cur.fetchone()

            # while row is not None:
            id = row[0]
            cur.close()
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
        finally:
            if conn is not None:
                conn.close()
        print("Found player with Id: {}".format(id))
        # return json.dumps(requested_person.as_dict(), indent=4, sort_keys=True, default=str)
        return {"playerId": id}

    def get_person(self, id):
        """ query data from the vendors table """
        teams = []
        requested_person = person.person()

        try:
            db_service = postgres_service.PostgresService()
            DATABASE_URL = db_service.get_connection_string()
            conn = psycopg2.connect(DATABASE_URL, sslmode='require')

            query = "SELECT playerid, birthyear, birthmonth, birthday, birthcountry, birthstate, birthcity, "
            query += "deathyear, deathmonth, deathday, deathcountry, deathstate, deathcity, namefirst, namelast, "
            query += "namegiven, weight, height, bats, throws, debut, finalgame, retroid, bbrefid, birth_date, "
            query += "debut_date, finalgame_date, death_date "
            query += "FROM public.people WHERE playerid = '{}';".format(id)

            cur = conn.cursor()
            cur.execute(query)
            row = cur.fetchone()

            # while row is not None:
            requested_person.id = row[0]
            requested_person.birthyear = row[1]
            requested_person.birthmonth = row[2]
            requested_person.birthday = row[3]
            requested_person.birthcountry = row[4]
            requested_person.birthstate = row[5]
            requested_person.birthcity = row[6]
            requested_person.deathyear = row[7]
            requested_person.deathmonth = row[8]
            requested_person.deathday = row[9]
            requested_person.deathcountry = row[10]
            requested_person.deathstate = row[11]
            requested_person.deathcity = row[12]
            requested_person.namefirst = row[13]
            requested_person.namelast = row[14]
            requested_person.namegiven = row[15]
            requested_person.weight = row[16]
            requested_person.height = row[17]
            requested_person.bats = row[18]
            requested_person.throws = row[19]
            requested_person.debut = row[20]
            requested_person.finalgame = row[21]
            requested_person.retroid = row[22]
            requested_person.bbrefid = row[23]
            requested_person.birth_date = row[24]
            requested_person.debut_date = row[25]
            requested_person.finalgame_date = row[26]
            requested_person.death_date = row[27]
            cur.close()
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
        finally:
            if conn is not None:
                conn.close()
        print("Found player with Id: {}".format(id))
        return requested_person.as_dict()

    def get_batting(self, id):
        """ query data from the vendors table """
        career_stats = []
        requested_hitter = hitter.hitter()
        print("Getting DB Connection")
        db_service = postgres_service.PostgresService()
        DATABASE_URL = db_service.get_connection_string()

        try:
            conn = psycopg2.connect(DATABASE_URL, sslmode='require')

            query = "SELECT id, playerid, yearid, stint, teamid, team_id, lgid, g, g_batting, ab, r, h, "
            query += "\"2B\", \"3B\", hr, rbi, sb, cs, bb, so, ibb, hbp, sh, sf, gidp "
            query += "FROM public.batting WHERE playerid = '{}';".format(id)
            print(query)

            cur = conn.cursor()
            cur.execute(query)
            # row = cur.fetchone()

            for row in cur:
                requested_hitter.id = row[0]
                requested_hitter.playerid = row[1]
                requested_hitter.yearid = row[2]
                requested_hitter.stint = row[3]
                requested_hitter.teamid = row[4]
                requested_hitter.lgid = row[5]
                requested_hitter.g = row[6]
                requested_hitter.g_batting = row[7]
                requested_hitter.ab = row[8]
                requested_hitter.r = row[9]
                requested_hitter.h = row[10]
                requested_hitter.doubles = row[11]
                requested_hitter.triples = row[12]
                requested_hitter.hr = row[13]
                requested_hitter.rbi = row[2]
                requested_hitter.sb = row[14]
                requested_hitter.cs = row[15]
                requested_hitter.bb = row[16]
                requested_hitter.so = row[17]
                requested_hitter.ibb = row[18]
                requested_hitter.hbp = row[19]
                requested_hitter.sh = row[20]
                requested_hitter.sf = row[21]
                requested_hitter.gidp = row[22]
                career_stats.append(requested_hitter.as_dict())
                requested_hitter = hitter.hitter()
            cur.close()
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
        finally:
            if conn is not None:
                conn.close()
        print("Found player with Id: {}".format(id))
        return career_stats

    def get_fielding(self, id):
        """ query data from the vendors table """
        career_stats = []
        requested_fielder = fielder.Fielder()
        db_service = postgres_service.PostgresService()
        DATABASE_URL = db_service.get_connection_string()
        try:
            conn = psycopg2.connect(DATABASE_URL, sslmode='require')

            query = "SELECT id, playerid, yearid, stint, teamid, team_id, lgid, pos, g, gs, innouts, "
            query += "po, a, e, dp, pb, wp, sb, cs, zr "
            query += "FROM public.fielding where playerid = '{}'".format(id)

            cur = conn.cursor()
            cur.execute(query)

            for row in cur:
                requested_fielder.id = row[0]
                requested_fielder.playerid = row[1]
                requested_fielder.yearid = row[2]
                requested_fielder.stint = row[3]
                requested_fielder.teamid = row[4]
                requested_fielder.team_id = row[5]
                requested_fielder.lgid = row[6]
                requested_fielder.pos = row[7]
                requested_fielder.g = row[8]
                requested_fielder.gs = row[9]
                requested_fielder.innouts = row[10]
                requested_fielder.po = row[11]
                requested_fielder.a = row[12]
                requested_fielder.e = row[13]
                requested_fielder.dp = row[14]
                requested_fielder.pb = row[15]
                requested_fielder.wp = row[16]
                requested_fielder.sb = row[17]
                requested_fielder.cs = row[18]
                requested_fielder.zr = row[19]
                career_stats.append(requested_fielder.as_dict())
                requested_fielder = fielder.Fielder()

            cur.close()
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
        finally:
            if conn is not None:
                conn.close()
        return career_stats
