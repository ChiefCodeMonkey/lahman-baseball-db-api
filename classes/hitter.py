class hitter:
    def __init__(self):
        self.id = 0
        self.playerid = ""
        self.yearid = 0
        self.stint = 0
        self.teamid = 0
        self.lgid = 0
        self.g = 0
        self.g_batting = 0
        self.ab = 0
        self.r = 0
        self.h = 0
        self.doubles = 0
        self.triples = 0
        self.hr = 0
        self.rbi = 0
        self.sb = 0
        self.cs = 0
        self.bb = 0
        self.so = 0
        self.ibb = 0
        self.hbp = 0
        self.sh = 0
        self.sf = 0
        self.gidp = 0


    def as_dict(self):
        return {
            "id": self.id,
            "playerId" : self.playerid,
            "yearId" : self.yearid,
            "stint" : self.stint,
            "teamId" : self.teamid,
            "leagueId" : self.lgid,
            "games" : self.g,
            "gamesAsHitter" : self.g_batting,
            "AB" : self.ab,
            "runs" : self.r,
            "hits" : self.h,
            "doubles" : self.doubles,
            "triples" : self.triples,
            "hr" : self.hr,
            "rbi" : self.rbi,
            "sb" : self.sb,
            "cs" : self.cs,
            "bb" : self.bb,
            "so" : self.so,
            "ibb" : self.ibb,
            "hbp": self.hbp,
            "sh" : self.sh,
            "sf" : self.sf,
            "gidp" : self.gidp
        }

