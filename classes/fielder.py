class Fielder:
    def __init__(self):
        self.id = 0
        self.playerid = ""
        self.yearid = ""
        self.stint = 0
        self.teamid = 0
        self.team_id = 0
        self.lgid = 0
        self.pos = ""
        self.g = 0
        self.gs = 0
        self.innouts = 0
        self.po = 0
        self.a = 0
        self.e = 0
        self.dp = 0
        self.pb = 0
        self.wp = 0
        self.sb = 0
        self.cs = 0
        self.zr = 0

    def as_dict(self):
        return {
            "id": self.id,
            "playerId": self.playerid,
            "yearId": self.yearid,
            "stint": self.stint,
            "teamId": self.teamid,
            "team_id": self.team_id,
            "lgId": self.lgid,
            "pos": self.pos,
            "g": self.g,
            "gs": self.gs,
            "inouts": self.innouts,
            "po": self.po,
            "a": self.a,
            "e": self.e,
            "dp": self.dp,
            "pb": self.pb,
            "wp": self.wp,
            "sb": self.sb,
            "cs": self.cs,
            "zr": self.zr
        }
