import swag as swag
from flask import Flask, jsonify, request, Response
from flasgger import Swagger, swag_from
from Service import people_service, team_service
from flask_cors import CORS
import psycopg2
import json
import os


app = Flask(__name__)
CORS(app)

app.config['SWAGGER'] = {
    "swagger_version": "2.0",
    "title": "The Baseball API",
    "headers": [
        ('Access-Control-Allow-Origin', '*'),
        ('Access-Control-Allow-Methods', "GET, POST, PUT, DELETE, OPTIONS"),
        ('Access-Control-Allow-Credentials', "true"),
    ],
    "specs": [
        {
            "version": "0.0.1",
            "title": "The Baseball API",
            "endpoint": 'v1_spec',
            "description": 'This is the version 1 of our API',
            "route": '/v1/spec',
            "license": {
                "name": "Apache 2.0",
                "url": "http://www.apache.org/licenses/LICENSE-2.0.html"
            },
        },
    ]
}

swagger = Swagger(app)


@swagger.definition('Teams')
class Teams(object):
    """
    Teams Object
    ---
    properties:
        name:
            type: string
    """
    def __init__(self, name):
        self.name = str(name)

    def dump(self):
        return dict(vars(self).items())


# Team Routes
@app.route("/v1/teams/<league>")
@swag_from()
def teams(league):
    """The teams endpoint returns all of the teams from the database.
    ---
    tags:
          - Teams
    parameters:
      - name: league
        in: path
        type: string
        enum: ['ALL', 'AA', 'FL', 'NA', 'PL', 'UA', 'NL', 'AL']
        required: true
        default: all
    definitions:
      Teams:
        type: object
        properties:
          team:
            type: array
    responses:
      200:
        description: A list of teams (may be filtered by league)
        schema:
            type: array
            items:
                $ref: '#/definitions/Teams'
      204:
        description: No teams were found in the db for the provided league id.

    definitions:
      Teams:
        type: "array"
        items:
          type: object
          properties:
              teamName:
                  type: "string"
        xml:
          name: "Teams"

    """
    teams = []
    query = ""
    status = 200
    try:
        conn = psycopg2.connect(
            host="localhost",
            database="lahmansDB",
            user="api",
            password="iop098*()")
        cur = conn.cursor()
        if league == 'ALL'.lower():
            query = "select distinct name from teams;"
        else:
            query = "select distinct name from teams where lgid = '{}';".format(league)
        cur.execute(query)
        row = cur.fetchone()
        if row is None:
            status = 204
        while row is not None:
            teams.append(row)
            row = cur.fetchone()

        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
    return Response(json.dumps(teams), status=status)


@app.route("/getteam/<id>")
def get_team(id):
    """
    Get a team by the teams id.
    ---
    tags:
        - Teams
    parameters:
        - name : id
          in: path
          type: string
          required: true
    definitions:
        Team:
            type: object
    responses:
        200:
            description: A teams profile
        204:
            description: Team not found with provided Id.
    """
    service = team_service.TeamService()
    team = service.get_team_by_id(id)
    if team is not None:
        return Response(json.dumps(team.as_dict()), status=200)
    else:
        return Response(team, status=204)


# People Routes
@app.route("/getperson/<id>", methods=['GET'])
def get_person(id):
    """
    Get a person by their id.
    ---
    tags:
        - Person
    parameters:
        - name : id
          in: path
          type: string
          required: true
    definitions:
        Team:
            type: int

    responses:
        200:
            description: A persons profile
        204:
            description: Person not found with provided Id.
    """
    print("Id: " + id)
    player = None
    if request.method == 'GET':
        service = people_service.PeopleService()
        player = service.get_person(id)

    return jsonify(player)


@app.route("/searchperson", methods=['GET'])
def search_person():
    """Find a players Id by searching by first and last name.
    ---
    tags:
          - Person
    parameters:
      - in: query
        name: firstName
        schema:
          type: string
        description: First Name of Person
      - in: query
        name: lastName
        schema:
          type: string
        description: First Name of Person
    definitions:
      Person:
        type: object
        properties:
          person:
            type: array
    responses:
      200:
        description: A collection of potential baseball people ids
        schema:
            type: array
            items:
                $ref: '#/definitions/Person'
      204:
        description: No people could be found using those filters.

    definitions:
      Person:
        type: "array"
        items:
          type: object
          properties:
              playerId:
                  type: "string"
        xml:
          name: "Person"

    """
    player_id = None

    if request.method == 'GET':
        first_name = request.args.get('firstName')
        last_name = request.args.get('lastName')

        service = people_service.PeopleService()
        player_id = service.search_by_name(first_name, last_name)
        return jsonify(player_id)
    else:
        return []


@app.route("/getHittingStats/<id>")
def get_hitting_stats(id):
    service = people_service.PeopleService()
    player = service.get_batting(id)
    return jsonify(player)

@app.route("/getFieldingStats/<id>")
def get_fielding_stats(id):
    service = people_service.PeopleService()
    player = service.get_fielding(id)
    return jsonify(player)

@app.route("/v1/spec")
def spec():
    return jsonify()


@app.route('/v1')
def hello():
    return "Learn more at https://www.chiefcodemonkey.com!"


@app.route('/v1/about')
def about():
    return "About this API"


if __name__ == '__main__':
 #   app.register_blueprint(swaggerui_blueprint, url_prefix=SWAGGER_URL)
    PORT = int(os.environ.get('PORT', 5000))
    app.run(port=PORT)
