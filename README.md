# The Baseball API

Python Flask API that sits in front of the Lahman Basball PostgreSQL DB


**The Data Source**

Reference: http://www.seanlahman.com/files/database/readme2017.txt

We are proud supporters of Sean Lahmans Baseball Archive. Learn more here: http://www.seanlahman.com/baseball-archive/


**Local Setup**

**Create Virtual Env

pip3 install virtualenv
python -m venv [directory]

*Linux/Mac
$ source venv/bin/activate

*Windows
C:\> vevn\Scripts\activate.bat

**Deactivate Python venv
$ deactivate
C:\> deactivate







To run locally, you will need one of the following:

1) A local.config file with the following info:
   [postgres]
    url = {{Postgres URL}}
2) An environment variable DATABASE_URL that contains the url to your Postgres DB
