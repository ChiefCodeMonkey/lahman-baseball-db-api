import psycopg2
from data.postgres_service import PostgresService
from classes import team


class TeamService:

    def _init__(self):
        pass

    def get_team_by_id(self, id):
        # Function to get team information based on a lahmens Id.
        status = 200
        query = "SELECT id, yearid, lgid, teamid, franchid, divid, div_id, teamrank, g, ghome, w, l, divwin, " \
                "wcwin, lgwin, wswin, r, ab, h, \"2B\", \"3B\", hr, bb, so, sb, cs, hbp, sf, ra, er, era, cg, sho, " \
                "sv, ipouts, ha, hra, bba,soa, e, dp, fp, name, park, attendance, bpf, ppf, teamidbr, " \
                "teamidlahman45, teamidretro FROM public.teams where teamid = '{}';".format(id)
        requested_team = team.team()
        try:
            db_service = PostgresService()
            DATABASE_URL = db_service.get_connection_string()
            conn = psycopg2.connect(DATABASE_URL, sslmode='require')

            cur = conn.cursor()
            cur.execute(query)
            row = cur.fetchone()

            if row is None:
                status = 204
                requested_team = None
            else:
                requested_team.id = row[0]
                requested_team.yearid = row[1]
                requested_team.lgid = row[2]
                requested_team.teamid = row[3]
                requested_team.franchid = row[4]
                requested_team.divid = row[5]
                requested_team.teamrank = row[6]
                requested_team.g = row[7]
                requested_team.ghome = row[8]
                requested_team.w = row[9]
                requested_team.l = row[10]
                requested_team.divwin = row[11]
                requested_team.wcwin = row[12]
                requested_team.r = row[13]
                requested_team.ab = row[14]
                requested_team.h = row[15]
                requested_team.doubles = row[16]
                requested_team.triples = row[17]
                requested_team.hr = row[18]
                requested_team.bb = row[19]
                requested_team.so = row[20]
                requested_team.sb = row[21]
                requested_team.cs = row[22]
                requested_team.hbp = row[23]
                requested_team.sf = row[24]
                requested_team.ra = row[25]
                requested_team.er = row[26]
                requested_team.era = row[27]
                requested_team.cg = row[28]
                requested_team.sho = row[29]
                requested_team.sv = row[30]
                requested_team.ipouts = row[31]
                requested_team.ha = row[32]
                requested_team.hra = row[33]
                requested_team.bba = row[34]
                requested_team.soa = row[35]
                requested_team.e = row[36]
                requested_team.db = row[37]
                requested_team.fp = row[38]
                requested_team.name = row[39]
                requested_team.park = row[40]
                requested_team.attendance = row[41]
                requested_team.bpf = row[42]
                requested_team.ppf = row[43]
                requested_team.teamidbr = row[44]
                requested_team.teamidlahman45 = row[45]
                requested_team.teamidretro = row[46]
            cur.close()
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
        finally:
            if conn is not None:
                conn.close()
        print("Found player with Id: {}".format(id))

        return requested_team
