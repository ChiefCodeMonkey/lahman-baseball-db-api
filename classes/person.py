
class person:
    def __init__(self):
        self.playerid = 0
        self.birthyear = 0
        self.birthmonth = 0
        self.birthday = 0
        self.birthcountry = ""
        self.birthstate = ""
        self.birthcity = ""
        self.deathyear = 0
        self.deathmonth = 0
        self.deathday = 0
        self.deathcountry = 0
        self.deathstate = 0
        self.deathcity = ""
        self.namefirst = ""
        self.namelast = ""
        self.namegiven = ""
        self.weight = 0
        self.height = 0
        self.bats = ""
        self.throws = ""
        self.debut = ""
        self.finalgame = ""
        self.retroid = ""
        self.bbrefid = ""
        self.brith_date = ""
        self.debut_date = ""
        self.finalgame_date = ""
        self.death_date = ""


    def as_dict(self):
        return {
            "playerId": self.playerid,
            "birthYear": self.birthyear,
            "birthMonth": self.birthmonth,
            "birthDay" : self.birthday,
            "birthCountry" : self.birthcountry,
            "birthState" : self.birthstate,
            "birthCity" : self.birthcity,
            "deathYear": self.deathyear,
            "deathMonth" : self.deathmonth,
            "deathDay" : self.deathday,
            "deathCountry" : self.deathcountry,
            "deathState" : self.deathstate,
            "deathCity" : self.deathcity,
            "nameFirst" : self.namefirst,
            "nameLast" : self.namelast,
            "nameGiven" : self.namegiven,
            "weight" : self.weight,
            "height" : self.height,
            "bats" : self.bats,
            "throws" : self.throws,
            "debut" : self.debut,
            "finalGame" : self.finalgame,
            "retroid" : self.retroid,
            "bbrefid" : self.bbrefid,
            "birthDate" : self.brith_date,
            "debutDate" : self.debut_date,
            "finalGameDate" : self.finalgame_date,
            "deathDate" : self.death_date
        }


