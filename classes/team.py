
class team:
    def __init__(self):
        self.id = 0
        self.yearid = 0
        self.lgid = ""
        self.teamid = ""
        self.franchid = ""
        self.divid = ""
        self.teamrank = ""
        self.g = 0
        self.ghome = 0
        self.w = 0
        self.l = 0
        self.divwin = ""
        self.wcwin = ""
        self.r = 0
        self.ab = 0
        self.h = 0
        self.doubles = 0
        self.triples = 0
        self.hr = 0
        self.bb = 0
        self.so = 0
        self.sb = 0
        self.cs = 0
        self.hbp = 0
        self.sf = 0
        self.ra = 0
        self.er = 0
        self.era = 0
        self.cg = 0
        self.sho = 0
        self.sv = 0
        self.ipouts = 0
        self.ha = 0
        self.hra = 0
        self.bba = 0
        self.soa = 0
        self.e = 0
        self.db = 0
        self.fp = 0
        self.name = ""
        self.park = ""
        self.attendance = 0
        self.bpf = 0
        self.ppf = 0
        self.teamidbr = ""
        self.teamidlahman45 = ""
        self.teamidretro = ""

    def as_dict(self):
        return {
            "id" : self.id,
            "yearId" : self.yearid,
            "leagueId": self.lgid,
            "teamId" : self.teamid,
            "franchiseId": self.franchid,
            "divisionId" : self.divid,
            "teamRank" : self.teamrank,
            "games" : self.g,
            "homeGames" : self.ghome,
            "wins" : self.w,
            "losses" : self.l,
            "divisionWins" : self.divwin,
            "wildCardWins" : self.wcwin,
            "runs" : self.r,
            "atBats" : self.ab,
            "hits" : self.h,
            "doubles" : self.doubles,
            "triples" : self.triples,
            "homeRuns": self.hr,
            "BB" : self.bb,
            "strikeOuts" : self.so,
            "stolenBases" : self.sb,
            "caughtStealing" : self.cs,
            "hitByPitch" : self.hbp,
            "sacFlies" : self.sf,
            "runsAgains": self.ra,
            "earnedRuns": self.er,
            "ERA": self.era,
            "completeGames" : self.cg,
            "shutOuts": self.sho,
            "saves" : self.sv,
            "ipOuts" : self.ipouts,
            "ha": self.ha,
            "hra": self.hra,
            "bba": self.bba,
            "soa": self.soa,
            "e": self.e,
            "db": self.db,
            "fp": self.fp,
            "name": self.name,
            "park": self.park,
            "attendance": self.attendance,
            "bpf": self.bpf,
            "ppf": self.ppf,
            "teamIbr": self.teamidbr,
            "teamIdLahman45": self.teamidlahman45,
            "teamIdRetro": self.teamidretro
        }