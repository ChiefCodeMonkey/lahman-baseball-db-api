import os
import configparser


class PostgresService:
    def __init__(self):
        self.DATABASE_URL = ""

    def get_connection_string(self):
        print("In DB Service")
        try:
            self.DATABASE_URL = os.environ['DATABASE_URL']
            print(self.DATABASE_URL)
        except Exception as error:
            print("IN Exception Case")
            config = configparser.ConfigParser()
            config.read('local.ini')
            self.DATABASE_URL = config['postgres']['url']

        print("URL: " + self.DATABASE_URL)
        return self.DATABASE_URL
